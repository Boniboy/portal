<?php
class CustomValidatedFile extends sfValidatedFile
{
  //a function that generates the name of the file
  public function generateFilename()
  {
   // return sha1("My_custom".rand(11111, 99999)).$this->getExtension($this->getOriginalExtension());
	// return $this->getOriginalName().$this->getExtension($this->getOriginalExtension());
	///
	if(file_exists($this->getPath().$this->getOriginalName()))
	{
	 return $this->appendToName($this->getOriginalName());
	}
	else{
	return $this->getOriginalName();
	}
  }
  //a custom method that appends number to a file name if it exists
  public function appendToName($file, $index = 0)
  {
   $newname = pathinfo($file->getOriginalName(), PATHINFO_FILENAME).$index.$file->getExtension();
   ///
   if(file_exists($file->getPath().$newname))
   {
    return $this->appendToName($file, ++$index);
   }
   else
   {
    return $newname;
   }
  }
} 
?>