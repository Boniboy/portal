<?php

/**
 * EIAProjectImpactPassTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class EIAProjectImpactPassTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object EIAProjectImpactPassTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('EIAProjectImpactPass');
    }
	public function getUserLetters()
	{
		$q=$this->createQuery('i')
		->leftJoin('i.EIAProjectDetail d')
		->where('d.created_by = ?',sfContext::getInstance()->getUser()->getGuardUser()->getId());
		
		return $q->fetchArray(); 
	}
		
}