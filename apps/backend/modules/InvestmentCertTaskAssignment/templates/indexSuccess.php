<?php //if($sf_user->hasCredential('assignJob') && $sf_user->hasCredential('investmentsupervisros')): ?>
<div id="page" class="dashboard">
 <div class="row-fluid">
       <div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER-->
						<!-- END STYLE CUSTOMIZER-->    	
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
						<h3 class="page-title">
							<?php echo __('Managers Account') ?>
							<small><?php echo __('Assign Tasks and Manage User Accounts') ?></small>
						</h3>
							<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="#"><?php echo __('Dashboard') ?></a> <span class="divider">/</span>
							</li>
							<li>
							<i class="icon-desktop"></i>
							<a href="#"><?php echo __('Manager') ?></a></li> <span class="divider">/</span>
							<li>
							<li>
							<i class="icon-desktop"></i>
							<a href="#"><?php echo __('Task Assignment') ?></a></li> <span class="divider">/</span>
							<li class="pull-right dashboard-report-li">
							<i class="icon-time"></i>
				              <?php echo __('Logged in on') ?> <font color="blue">
									<?php
                                       $date = date("F j, Y");
									   print $date;
									?>
									</font>
							</li>
							
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
					
					<div class="span11">
							<!-- BEGIN RECENT ORDERS PORTLET-->
							<div class="widget">
								<div class="widget-title">
									<h4><?php echo __('Investment Certificates - Task Assignment Record') ?></h4>						
								</div>
								<div class="widget-body">
								<?php if(count($task_assignments) != null) : ?>
									<table class="table table-striped table-bordered" id="tasks_monitor">
										<thead>
											<tr>
												<th><?php echo __('Officer') ?></th>
												<th></i><?php echo __('For Business') ?></th>
												<th><?php echo __('Reference No') ?></th>
												<th><?php echo __('Instructions') ?></th>
												<th><?php echo __('Status') ?></th>
												<th><?php echo __('Due Date') ?></th>
												<th><?php echo __('Date Assigned') ?></th>
												<th><?php echo __('Actions') ?></th>
											</tr>
										</thead>
										<tbody>
										  <?php foreach($task_assignments as $available): ?>
											<tr class="odd gradeX">
												<td class="highlight">
												<?php echo $available['first_name']."\t".$available['last_name'] ?></td>
												<td><?php echo $available['name'] ?></td>
												<td><?php echo $available['applicant_reference_number']?> </td>
												<td> <?php echo html_entity_decode($available['instructions']) ?> </td>
												<td> 
												<!-- we are going to mark this red if not started -->
												<?php  
												$status =  $available['work_status'] ;
												?> 
												<?php if($status == "notstarted"): ?>
												  <font color="red"><?php echo $status; ?></font>
												<?php endif; ?>
												<!-- else -->
												<?php if($status != "notstarted"): ?>
												  <font color="green"><?php echo $status; ?></font>
												<?php endif; ?>
												</td>
												<td> <?php echo $available['duedate'] ?> </td>
												<td> <?php echo $available['created_at'] ?> </td>
												
												<td> 
												<?php if($status !='reporting'): ?>
												<?php if($status !='awaitingpayment'): ?>
												 <?php if($status !='complete'): ?>
												 <?php if($status !='rejected'): ?>
												  <?php if($status !='started'): ?>
												  <?php if($status !='paymentconfirmed'): ?>
												 <a href="<?php echo url_for('InvestmentCertTaskAssignment/edit?id='.$available['id'])?>"><?php echo __('Edit') ?>
												 
												 </a>
												     <?php endif; ?>
													 <?php endif; ?>
													 <?php endif; ?>
												 <?php endif; ?>
												<?php endif; ?>
												
												<?php endif; ?>
												</td>
												
											</tr>
										<?php endforeach;?>	
										
										</tbody>
									</table>
								<?php endif; ?>
							    <?php if(count($task_assignments) <= 0): ?>
								<div class="alert alert-info">
										<strong><?php echo __('Information') ?>!</strong> <br/><?php echo __('Sorry, I found no record to display')?>. 
								</div>
							    <?php  endif; ?>
								<?php //if(count($task_assignments) > 0): ?>
									<!--<div class="space7"></div>
									<div class="clearfix">
										<a href="<?php //echo url_for('InvestmentCertTaskAssignment/new') ?>" class="btn btn-primary"><?php //echo __('Assign Tasks') ?></a>
									</div> -->
								 <?php  //endif; ?>
								</div>
							</div>
							<!-- END RECENT ORDERS PORTLET-->
						</div>


  </div>
  </div>
<?php //endif; ?>