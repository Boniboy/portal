<?php

/**
 * auditlogs actions.
 *
 * @package    rdbeportal
 * @subpackage auditlogs
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class auditlogsActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    //$this->forward('default', 'module');
	$this->logs = Doctrine_Core::getTable('AuditLogs')->retrieveLogs();
  }
}
