<div class="row-fluid">
 <div class="span11">
      <div class="widget">
								<div class="widget-title">
									<h4><?php echo __('System Log Monitor - View Users Actions i.e. Objects and Methods accessed by users') ?></h4>						
								</div>
								<div class="widget-body">
								<?php if(count($logs) != null) : ?>
									<table class="table table-striped table-bordered" id="inboxbackend">
										<thead>
											<tr>
												<th><?php echo __('UserName') ?></th>
												<th></i><?php echo __('Object Accessed') ?></th>
												<th><?php echo __('Method/Action Performed') ?></th>
												<th><?php echo __('Date') ?></th>
											</tr>
										</thead>
										<tbody>
										  <?php foreach($logs as $log): ?>
											<tr class="odd gradeX">
											    <td> <?php echo $log['user'] ?> </td>
												<td> <font color="green"><?php echo $log['object_name'] ?> </font></td>
												<td> <font color="red"><?php echo $log['method'] ?> </font></td>
												<td> <?php 
												$date = new DateTime($log['created_at']);
												echo $date->format('g:ia \o\n l jS F Y');
												 ?> </td>	
											</tr>
										<?php endforeach;?>	
										
										</tbody>
									</table>
								<?php endif; ?>
							    <?php if(count($logs) <= 0): ?>
								<div class="alert alert-info">
										<strong><?php echo __('Information') ?>!</strong> <br/><?php echo __('Sorry, No System Log Files Available')?>. 
								</div>
							    <?php  endif; ?>
								
								</div>
							</div>
 </div>
</div>