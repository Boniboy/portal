<?php decorate_with(dirname(__FILE__).'/defaultLayout.php') ?>
<body>
<div class="wrapper">

	<div class="mainWrapper">
        <div class="leftHolder">
        	<a href="http://rdb.gov.rw" title="RWANDA DEVELOPMENT BOARD" class="logo">RDB Rwanda</a>
             <center><?php echo image_tag('/sf/sf_default/images/icons/lock48.png', array('alt' => 'credentials required', 'class' => 'sfTMessageIcon', 'size' => '100x100')) ?></center>
        </div>
        <div class="rightHolder">
            <div class="message"><p>
			                        <?php echo __('Sorry, Access Denied. Invalid Credentials. To Access this page, Contact the System Administrator rdbadmin@rdb.gov.rw')?>
			</p></div>
            <div class="robotik"><img src="/css/error/css/images/robotik.png" alt="Oooops....we can’t find that page." title="Oooops....we can’t find that page." id="robot"></div>
            <div class="tryToMessage">
			    <h2><u><?php echo __('Hint:') ?></u></h2>
				<p><?php echo __('This section is restricted to users with certain permissions .') ?></p> <br/>
                <?php echo __('Whats Next:') ?>
			 <a href="<?php echo url_for('sf_guard_signout')?>" ><button type="button" class="btn btn-info"><?php echo __('Back') ?></button></a>
                <ul>
                    <li>
                    <li><?php echo __('You can also email us at if you think this is an error at') ?> <?php echo mail_to('support@rdb.rw')?></li>
                </ul>
            </div>
          </div>
      


        <footer>
        <p class="copy"><?php 
				   $date = date('Y') ;
				   echo $date;
				?>
				<?php echo  __('&copy; Rwanda Development Board. All Rights Reserved.') ?></p>
        <menu>
			<li><a href="#" title="Support"><?php echo __('Support') ?></a></li>
           
        </menu>
        </footer>
        <!-- end footer -->

	</div>

</div>
</body>