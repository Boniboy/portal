<?php

/**
 * messages actions.
 *
 * @package    rdbeportal
 * @subpackage messages
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class messagesActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    /*$this->messagess = Doctrine_Core::getTable('Messages')
      ->createQuery('a')
      ->execute(); */
	  $username = sfContext::getInstance()->getUser()->getGuardUser()->getUsername();
	  //a user can only see messages sent to him/her
	  $this->messagess = Doctrine_Core::getTable('Messages')->retrieveAllMessages($username);
	  //print_r($this->messagess); exit;
	  
	  ///get messages sent by the current logged user
	  $this->sent_messages = Doctrine_Core::getTable('Messages')->retrieveSentMessages($username);
  }

  public function executeShow(sfWebRequest $request)
  {
    $this->messages = Doctrine_Core::getTable('Messages')->find(array($request->getParameter('id')));
	//we will update the message table with 1 to indicate that the message has been read by the 
	//intended recipient.
	$message_id = $request->getParameter('id');
	$update_message_status = Doctrine_Core::getTable('Messages')->updateMessageStatus($message_id);
    $this->forward404Unless($this->messages);
  }

  public function executeNew(sfWebRequest $request)
  {
   //  $param = $request->getParameter('request_type'); 
	//if we have a parameter we retrive it and use it to set receipient of message we want the id of business 
	//from that we retrieve the user who assigned this account
	$assignee = Doctrine_Core::getTable('TaskAssignment')->getUserAssignedTask($request->getParameter('business'));
	$applicant_ref = Doctrine_Core::getTable('TaskAssignment')->getApplicantReference($request->getParameter('business'));
	 //if this user had previously sent a request for accepting this applicant certificate application, we delete it where
	 //the status is decline. This is to prevent the system from throwing an exception
//	 $delete_query = Doctrine_Core::getTable('InvestmentRequests')->deleteRecord($applicant_ref);
	 
	
	
	$user_request = $request->getParameter('request_type');
	/////
	$this->getUser()->setAttribute('item_session2', $applicant_ref);
	//$this->getUser()->setAttribute('ref',$applicant_ref);
	//print $applicant_ref; exit;
	///
	//$this->getUser()->setAttribute('item_session', $request->getParameter('request_type'));
	//print $this->getUser()->getAttribute('item_session'); exit;
	///use case
	   switch($user_request){
	    case "accept_application":
		 //setting a session 
			$this->getUser()->setAttribute('recepient',$assignee);
			$this->getUser()->setAttribute('request_type', "accept_application");
			$this->getUser()->setAttribute('message_subject',"Request to Accept Investment Application for ".$applicant_ref);
			
			//print $this->getUser()->getAttribute('request_type') ; exit;
			///
			$this->getUser()->setAttribute('item_session', $request->getParameter('request_type'));
			//$this->getUser()->setAttribute('app_number', $applicant_ref);
		
			 $this->form = new MessagesForm();
		break;
	    case "decline_application":
		  //setting a session 
			$this->getUser()->setAttribute('recepient',$assignee);
		//	$this->getUser()->setAttribute('request_type', "decline_application");
			$this->getUser()->setAttribute('message_subject',"Request to Decline Investment Application for ".$applicant_ref);
			//$this->getUser()->setAttribute('applicant_ref_no',$applicant_ref);
			//print $this->getUser()->getAttribute('request_type') ; exit;
			//
			$this->getUser()->setAttribute('item_session', $request->getParameter('request_type'));
			
			$this->form = new MessagesForm();
		break;
		default:
		// $this->getUser()->getAttributeHolder()->remove('request_type');
		  $this->getUser()->getAttributeHolder()->remove('app_number');
		  $this->getUser()->getAttributeHolder()->remove('message_subject');
		  $this->getUser()->getAttributeHolder()->remove('recepient'); 
		  $this->form = new MessagesForm();
	   
	   }
	///end case
	
  }
  //method to set session
 /* public function MySessionVariable($applicant_ref)
  {
    $applicant_ref_no = $this->getUser()->setAttribute('applicant_reference_number', $applicant_ref);
	return $applicant_ref_no;
  } */
  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));
    ///
	//print "request is".$this->getUser()->getAttribute('app_number'); exit;
    $this->form = new MessagesForm();
    //// 
    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
	if($request->getParameter('user')== 'dataAdmin')
	{
		$this->message=Doctrine_Core::getTable('Messages')->getMessageInvestor();
	}else
	{
		$this->message=Doctrine_Core::getTable('Messages')->getEditMessage();
	}
    $this->forward404Unless($messages = Doctrine_Core::getTable('Messages')->find(array($request->getParameter('id'))), sprintf('Object messages does not exist (%s).', $request->getParameter('id')));
    $this->form = new MessagesForm($messages);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($messages = Doctrine_Core::getTable('Messages')->find(array($request->getParameter('id'))), sprintf('Object messages does not exist (%s).', $request->getParameter('id')));
    $this->form = new MessagesForm($messages);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($messages = Doctrine_Core::getTable('Messages')->find(array($request->getParameter('id'))), sprintf('Object messages does not exist (%s).', $request->getParameter('id')));
    $messages->delete();

    $this->redirect('messages/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      
      // print $this->getUser()->getAttribute('request_type') ; exit;
      //$this->redirect('messages/edit?id='.$messages->getId());
	//  print $this->getUser()->getAttribute('request_type'); exit;
	 ///
	  //we use case
	  switch($this->getUser()->getAttribute('item_session'))
	  {
	    case "accept_application":
		 $messages = $form->save();
		// print "accept".$this->getUser()->getAttribute('applicant_ref_no'); exit;
	     /////
	     $request = new InvestmentRequests();
		 $request->requestor = $this->getUser()->getGuardUser()->getId();
		 $request->request_type = $this->getUser()->getAttribute('item_session');
		 $request->status = "notset";
		 $request->reference_number = $this->getUser()->getAttribute('item_session2');
		 ///
		 $request->save();
		 ////remove all atributes
		  $this->getUser()->getAttributeHolder()->remove('item_session');
		 // $this->getUser()->getAttributeHolder()->remove('app_number');
		  $this->getUser()->getAttributeHolder()->remove('message_subject');
		  $this->getUser()->getAttributeHolder()->remove('recepient');
		  //
		   if($messages->getRecepientEmail() && $messages->getMessage())
			  {
			  $recepients=Doctrine_Core::getTable('sfGuardUser')->findByEmailAddress($messages->getRecepientEmail());
			  $emails=array();
			  $emails[]=$recepients[0]['outlook_address'];
	  		  $emails[]=$messages->getRecepientEmail();
			  $this->getMailer()->composeAndSend('noreply@rdb.com',$emails ,$messages->getMessageSubject(),
									"A new message has been sent to your account.\n".
									 "Please login to your account to review it. Use the link below\n".
									 "http://198.154.203.38:8234/"
												  ); 
												  
				
			 } 
		  $this->redirect('dashboard/index');
		  break;
		case "decline_application":
	     $messages = $form->save();
	   // print "decline_application"; exit;
	     /////
	     $request = new InvestmentRequests();
		 $request->requestor = $this->getUser()->getGuardUser()->getId();
		 $request->request_type = $this->getUser()->getAttribute('item_session');
		 $request->status = "notset";
		 $request->reference_number = $this->getUser()->getAttribute('item_session2');
		 $request->save();
		 
		  $this->getUser()->getAttributeHolder()->remove('item_session');
		 // $this->getUser()->getAttributeHolder()->remove('app_number');
		 
		  $this->getUser()->getAttributeHolder()->remove('message_subject');
		  $this->getUser()->getAttributeHolder()->remove('recepient');
		  ////
		  if($messages->getRecepientEmail() && $messages->getMessage())
			  {
		  $recepients=Doctrine_Core::getTable('sfGuardUser')->findByEmailAddress($messages->getRecepientEmail());
			  $emails=array();
			  $emails[]=$recepients[0]['outlook_address'];
			  $emails[]=$recepients[0]['email_address'];
			  $this->getMailer()->composeAndSend('noreply@rdb.com',$emails ,$messages->getMessageSubject(),
									"A new message has been sent to your account at RDB Eportal.\n".
									 "Please login to your account to review it. Use the link below\n".
									 "http://198.154.203.38:8234/"
												  ); 
		     }
		  $this->redirect('dashboard/index');
		  break;
		default:
		 $messages = $form->save();
				  //we log all messages sent to a user
			  $messageLog = new MessageLogs();
			  $messageLog->message_sender = $messages->getSender();
			  $messageLog->message_recipient = $messages->getRecepient();
			  $messageLog->message = $messages->getMessage();
			  $messageLog->status = "delivered";
			  $messageLog->save();
		 $this->redirect('dashboard/index');
		//print $this->getUser()->getAttribute('applicant_ref_no'); exit;
	  }
	
    }
  }
	public function executeReply(sfWebRequest $request)
	{
		$message = new Messages();
		$message->recepient=$request->getParameter('recepient');
		$message->sender=sfContext::getInstance()->getUser()->getGuardUser()->getUsername();
		$message->sender_email=sfContext::getInstance()->getUser()->getGuardUser()->getEmailAddress();
		$message->recepient_email=$request->getParameter('email');
		$message->save();
		///
		 //we log all messages sent to a user
	  $messageLog = new MessageLogs();
	  $messageLog->message_sender = $message->getSender();
	  $messageLog->message_recipient = $message->getRecepient();
	  $messageLog->message = $message->getMessage();
	  $messageLog->status = "delivered";
      $messageLog->save();
		$messageId=Doctrine_Core::getTable('Messages')->getMessageId($request->getParameter('recepient'));
		$this->redirect('messages/edit?id='.$messageId[0]['id'].'&user=dataAdmin');
	}
	///////////////////////function for retrieve new messages for desktop notifications supported by chrome currently
public function executeRetrieve(sfWebRequest $request) 
  {
     /* We use the Symfony inbuilt method to get connection and retrieve data*/
				try {
				
				  $db = Doctrine_Manager::getInstance()->getCurrentConnection();
				  $userName  = sfContext::getInstance()->getUser()->getGuardUser()->getUsername();
				
				  //working if we select all
				  $data =  $db->fetchAssoc("SELECT count(messages.sender) as number FROM messages WHERE recepient = '$userName' and status = 0") ;
				 if($data != null)
				 {
				   $out = array('messages' => $data) ;
				   
				   echo(json_encode($out)); exit;
				  
				 }
				 if($data == null)
				 {
				   print "An error occured"; exit;
				 }
				 }
				catch (Exception $e) {
				  print 'Exception : ' . $e->getMessage();
				}
  }
 public function executeNotify(sfWebRequest $request) 
  {
     /* We use the Symfony inbuilt method to get connection and retrieve data*/
				try {
				
				  $db = Doctrine_Manager::getInstance()->getCurrentConnection();
				  $userName  = sfContext::getInstance()->getUser()->getGuardUser()->getUsername();
				
				  //working if we select all
				  $data =  $db->fetchAssoc("SELECT COUNT(notifications.message) as number
				  FROM notifications WHERE notifications.recepient = '$userName' and notifications.status = 0") ;
				 if($data != null)
				 {
				   $out = array('notify' => $data) ;
				   
				   echo(json_encode($out)); exit;
				  
				 }
				 if($data == null)
				 {
				   print "An error occured"; exit;
				 }
				 }
				catch (Exception $e) {
				  print 'Exception : ' . $e->getMessage();
				}
  } 
  //we automatically update notifications if seen by the user
  public function executeNotification(sfWebRequest $request) 
  {
   $update = Doctrine_Core::getTable('Notifications')->updateNotificationStatus($request->getParameter('id'));
   exit;
  }
	
}
