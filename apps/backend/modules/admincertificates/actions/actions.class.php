<?php

/**
 * admincertificates actions.
 *
 * @package    rdbeportal
 * @subpackage admincertificates
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class admincertificatesActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $this->forward('default', 'module');
  }
  public function executeInvestment(sfWebRequest $request)
  {
    //get submitted user applications for issuance of the Investment certificates
    $status = 'submitted';
	$this->new_applications = Doctrine_Core::getTable('InvestmentApplication')->getUnassignedApplications($status);
	//get all assigned tasks
	$this->task_assignments = Doctrine_Core::getTable('TaskAssignment')->getAllAssignedTasks();
  }
  public function executeEia(sfWebRequest $request)
  {
    //get EIA Applications not assigned to users
    $this->unassigned= Doctrine_Core::getTable('EIApplicationStatus')->getApplicationStatus('submitted');
	///
	 $this->ei_task_assignments = Doctrine_Core::getTable('EITaskAssignment')->getAllAssignTasks();
  }
}
