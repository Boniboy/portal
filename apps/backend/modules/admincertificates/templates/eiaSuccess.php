<div class="row-fluid">
 <div class="span11">
    <div class="widget">
								<div class="widget-title">
									<h4><?php echo __('Recent Applications for  EIA Certificates') ?></h4>						
								</div>
								<div class="widget-body">
								<?php if(count($unassigned) == 0 && count($assigning)==0): ?>
									<div class="alert alert-block alert-info fade in">
									<h4 class="alert-heading"><?php echo __('No recent application found') ?></h4>
									<p><?php echo __('Please try again later/ Refresh the page') ?> </p>
									</div>
								<?php endif; ?>
								<?php if(count($unassigned)>0): ?>
								  
									
									<table class="table table-striped table-bordered" id="eia_manager">
										<thead>
											<tr class="odd gradeX">
											  <th><?php echo __('Reference No.') ?> </th>
												<th><?php echo __('Title') ?></th>
												<th><?php echo __('Developer') ?></th>
												<th><?php echo __('Actions') ?></th>
											</tr>
										</thead>
										<tbody>
										<?php foreach($unassigned as $unassign): ?>
											<tr>
												<td><?php echo $unassign['project_reference_number'] ?></td>
												<td><?php echo $unassign['project_title'] ?> </td>
												<td><?php echo $unassign['developer_name'] ?></td>
												<td> <a href="<?php echo url_for('eiaTaskAssign/new?id='.$unassign['id']) ?>"><button class="btn btn-inverse"><i class="icon-refresh icon-white"></i> <?php echo __('Assign') ?></button></a></td>
									
											</tr>
										<?php endforeach; ?>	
										</tbody>
									</table>
									
								<?php endif; ?>
								<?php if(count($assigning)>0): ?>
								<?php //we will delete the session variable if any available incase this user fails to assign a task for a sessionid we have created
								  $session_eai_id = sfContext::getInstance()->getUser()->getAttribute('eiaprojectdetail_id') ;
								  // we clear the current variable
								  
								  
								  ?>
								
									
									<table class="table table-striped table-bordered" id="eia_manager">
										<thead>
											<tr class="odd gradeX">
											    <th><?php echo __('Reference No.') ?> </th>
												<th><?php echo __('Title') ?></th>
												<th><?php echo __('Developer') ?></th>
												<th><?php echo __('Actions') ?></th>
											</tr>
										</thead>
										<tbody>
										<?php foreach($assigning as $assign): ?>
											<tr>
												<td><?php echo $assign['project_reference_number']."".$session_eai_id ?></td>
												<td><?php echo $assign['project_title'] ?> </td>
												<td><?php echo $assign['developer_name'] ?></td>
												<td> <a href="<?php echo url_for('eiaTaskAssign/new?id='.$assign['id']) ?>"><button class="btn btn-inverse"><i class="icon-refresh icon-white"></i> 
												<?php echo __('Assign') ?></button></a></td>
									
											</tr>
										<?php endforeach; ?>	
										</tbody>
									</table>
									
								<?php endif; ?>
									<div class="space7"></div>
									<div class="clearfix">
										<a href="<?php echo url_for('eiaTaskAssign/index') ?>" class="btn btn-small btn-primary"><?php echo __('View All Assigned Tasks') ?></a>
									</div>
								</div>
							</div>
 </div>
</div>

<div class="row-fluid">
 <div class="span11">
    <div class="widget">
								<div class="widget-title">
									<h4><?php echo __('Task Monitor - EIA Certificate Log Status') ?></h4>						
								</div>
								<div class="widget-body">
								<?php if(count($ei_task_assignments) != null) : ?>
									<table class="table table-striped table-bordered" id="tasks_monitor">
										<thead>
											<tr>
												<th><i class="icon-user"></i> <span class="hidden-phone"><?php echo __('Assigned To') ?></span></th>
												<th><i class="icon-user"></i> <span class="hidden-phone"><?php echo __('Assigned By') ?></span></th>
												<th><span class="hidden-phone"><?php echo __('Project Name') ?></span></th>
												<th><span class="hidden-phone"><?php echo __('Work Status') ?></span></th>
												<th><span class="hidden-phone"><?php echo __('Due Date') ?></span></th>
											   <th><span class="hidden-phone"><?php echo __('Actions') ?></span></th>
											</tr>
										</thead>
										<tbody>
										  <?php foreach($ei_task_assignments as $available): ?>
											<tr class="odd gradeX">
											    <td class="highlight">
													<?php echo $available['first_name']."\t".$available['last_name'] ?></td>
											    <td><?php echo $available['updated_by']?> </td>
												<td><?php echo $available['name'] ?></td>
												<td> 
												<!-- we are going to mark this red if not started -->
												<?php  
												$status =  $available['work_status'] ;
												?> 
												<?php if($status == "notstarted"): ?>
												  <font color="red"><?php echo $status; ?></font>
												<?php endif; ?>
												<!-- else -->
												<?php if($status != "notstarted"): ?>
												  <font color="green"><?php echo $status; ?></font>
												<?php endif; ?>
												</td>
												<td> <?php echo $available['duedate'] ?> </td>
												<td> 
												 <?php if($status =='notstarted'): ?>
												 <a href="<?php echo url_for('eiaTaskAssign/edit?id='.$available['id'])?>"><?php echo __('Edit') ?></a>
												 <?php endif; ?>
												 <?php if($status !='notstarted'): ?>
												 <?php echo button_to('Contact assignee','eiaDataAdmin/message?applicant='.$available['username'],array('class' => 'btn btn-primary')) ?>
												 <?php endif; ?>
												</td>
												
											</tr>
										<?php endforeach;?>	
										
										</tbody>
									</table>
								<?php endif; ?>
							    <?php if(count($ei_task_assignments) <= 0): ?>
								<div class="alert alert-info">
										<strong><?php echo __('Information') ?>!</strong> <br/><?php echo __('Sorry, I found no record to display')?>. 
								</div>
							    <?php  endif; ?>
								
								</div>
							</div>
 </div>
  </div>