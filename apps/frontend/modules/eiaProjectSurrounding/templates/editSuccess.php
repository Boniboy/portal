<div class="row-fluid">
	<div class="span11">
		<div class="widget">
			<div class="widget-title">
				<h4>Project Surrounding</h4>
			</div>
			<div class="alert alert-block alert-info fade in">
				<p>
					<?php echo __('Please make changes below') ?>.
				</p>
			</div>
			<div class="widget-body">
			<?php include_partial('form', array('form' => $form)) ?>
			
			</div>
		</div>
	</div>
	
</div>
