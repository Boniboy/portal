<div class="row-fluid">
				<div class="widget">
				 <div class="widget-title">
					<h4><i class="icon-reorder"></i><?php echo __('EIA Certificates ---- Project Attachments') ?></h4>						
					</div>
            <div class="widget-body">
			<div class="alert alert-block alert-info fade in">
														
							
							<p>
								<?php echo __('Please make changes below.') ?>.
							</p>
						 </div>
			         <?php include_partial('form', array('form' => $form)) ?>
		    </div>			 

			</div>
			</div>
</div>
