<div class="row-fluid">
	<div class="span8">
		<div class="widget">
			<div class="widget-title">
				<h4> Environmental Impact Assessment --- Project Developer Details </h4>
			</div>
			<div class="widget-body">
			<div class="alert alert-block alert-info fade in">
				<p>
				<?php echo __('Please make changes below.') ?>.
				</p>
			</div>
			<?php include_partial('form', array('form' => $form)) ?>
			
			</div>
		</div>
	</div>
</div>
