<?php decorate_with(dirname(__FILE__).'/defaultLayout.php') ?>
<body>
<div class="wrapper">

	<div class="mainWrapper">
        <div class="leftHolder">
        	<a href="http://rdb.gov.rw" title="RWANDA DEVELOPMENT BOARD" class="logo">RDB Rwanda</a>
            <div class="errorNumber">404</div> 
        </div>
        <div class="rightHolder">
            <div class="message"><p><?php __('Sorry, The Item you are looking for is not available.') ?></p></div>
            <div class="robotik"><img src="/css/error/css/images/robotik.png" alt="Oooops....we can’t find that page." title="Oooops....we can’t find that page." id="robot"></div>
            <div class="tryToMessage">
			   <?php echo __('Hint:') ?>
				<ul>
				  <li><?php echo __('Did you type the URL? You may have typed the address (URL) incorrectly.') ?></li>
				</ul>
                <?php echo __('Whats Next:') ?>
			 <a href="<?php echo url_for('investmentapp/index') ?>" ><button type="button" class="btn btn-info"><?php echo __('Go to Dashboard') ?></button></a>
                <ul>
                    <li>
                    <li><?php echo __('You can also email us at if you think this is an error at')?> <?php echo mail_to('support@rdb.rw')?></li>
                </ul>
            </div>
          </div>
      


        <footer>
        <p class="copy"><?php 
				   $date = date('Y') ;
				   echo $date;
				?>
				<?php echo  __('&copy; Rwanda Development Board. All Rights Reserved.') ?></p>
        <menu>
            <li><a href="<?php echo url_for('investmentapp/index') ?>" title="Dashboard"><?php echo __('Dashboard')?></a></li>
			<li><a href="#" title="Support"><?php echo __('Support') ?></a></li>
           
        </menu>
        </footer>
        <!-- end footer -->

	</div>

</div>
</body>