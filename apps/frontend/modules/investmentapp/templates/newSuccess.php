
<?php //exit; ?>
<div class="row-fluid">
			  <div class="span8">
				<div class="widget">
				 <div class="widget-title">
					<h4><i class="icon-reorder"></i><?php echo __('Investment Certificates ---- Company Details Form') ?></h4>						
					</div>
            <div class="widget-body">
			<div class="alert alert-block alert-info fade in">
														
							<h4 class="alert-heading"><?php echo __('Step 1') ?></h4>
							<p>
								<?php echo __('Please Provide Below Fields to Start your application for investment certificate') ?> 
							</p>
						 </div>
						 <?php //exit; ?>
			         <?php include_partial('form', array('form' => $form)) ?>
		    </div>			 

			</div>
			</div>
			<div class="span4">
						
							
							<div class="widget">
											<div class="widget-title">
												<h4><i class="icon-bell"></i><?php echo __('Important') ?></h4>						
											</div>
											<div class="widget-body">
											<div class="alert alert-success">
										    <strong><?php echo __('To successfully submit this information, make sure that you have a valid business 
											registration number/ TIN Number. You company must be registered via the Business Registration System') ?></strong>
											</div>
											
											</div>
							</div>
			</div>
			<div class="span4">
			<div id='loading' class="alert alert-success" style='display:none;'>
			 
			 <div><p class="text-error">Connection to RDB OBR System established....</p> <span class="icon-spinner"></span></div>
			</div>
			<div id='loading2' class="alert alert-success" style='display:none;'>
			 <button class="close" data-dismiss="alert">×</button>
			 <div><p class="text-error">Request received..... </p> <span class="icon-spinner"></span></div>
			</div>
			<div id='loading3' class="alert alert-success" style='display:none;'>
			 
			 <div><p class="text-error">Processing request(Validating TIN Number) ....... </p> <span class="icon-spinner"></span></div>
			</div>
			 <div id='loadingError' class="alert alert-success" style='display:none;'>
			 
			 <div><p class="text-error"><font color="red">This Tin Number Is Invalid. Please Try again</font></p>
			 <p><font color="blue">If you think is an Error, Please Contact us</font> <a href="<?php echo url_for('messages/new') ?>"><button class="btn btn-small" type="button">Contact Support</button></a></p>
			 
			 </div>
			</div> 
			<div id='loadingSuccess' class="alert alert-success" style='display:none;'>
			 
			 <div><p class="text-success">Good, Your TIN Number is Valid</p></div>
			</div> 
			
			</div>
</div>
